# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [0.1.0] - 2018-06-15
### Added
- submodule buildroot
- .gitlab-ci.yml
- README.md
- CHANGELOG
- LICENSE

### Changed
- make -s to reduce log size.

### Removed
- nn
